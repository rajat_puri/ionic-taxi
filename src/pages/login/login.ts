import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Nav } from 'ionic-angular';
import { LoginService } from '../../providers/login/login';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookieService } from '../../app/cookie.service';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild(Nav) nav:Nav;
  loginForm: FormGroup;
  showErrorCode: any;
  loading: Boolean;
  default: any;
  retUrl: any;
  
  constructor(private alertCtrl: AlertController,public navCtrl: NavController,private fb: FormBuilder, public navParams: NavParams,public loginService:LoginService,public cookieService:CookieService){
    this.loginForm = fb.group({
      userStatus: '',
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  passwordNotValid: Boolean = false;
  emailNotValid: Boolean = false;
  authenticate() {
     console.log('inside authenticate::')
     console.log(this.loginForm.controls)
    // if(this.loginForm.value.userStatus == 0){
    if (!this.loginForm.controls.email.valid) {
      this.emailNotValid = true;
      return false;
    }
    if (!this.loginForm.controls.password.valid) {
      this.passwordNotValid = true;
      return false;
    }

    this.doLogin();
 
  }

   doLogin(){

    this.navCtrl.setRoot('MenuPage')
   }

}
