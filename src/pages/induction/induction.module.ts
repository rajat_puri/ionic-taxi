import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InductionPage } from './induction';

@NgModule({
  declarations: [
    InductionPage,
  ],
  imports: [
    IonicPageModule.forChild(InductionPage),
  ],
})
export class InductionPageModule {}
