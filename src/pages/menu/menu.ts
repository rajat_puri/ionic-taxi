import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Nav } from 'ionic-angular';
import { CookieService } from '../../app/cookie.service';
import { AlertController } from 'ionic-angular';
import { LoginService } from '../../providers/login/login';

 

export interface PageInterface{
  title:string;
  pageName:string;
  tabComponent?:any;
  index?:number;
  icon:string;
}

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  rootPage="HomePage";

  @ViewChild(Nav) nav:Nav;

  pages:PageInterface[]=[

    {title:"Home", pageName:"HomePage",icon:'home'},
    {title:"Profile", pageName:"ProfilePage",icon:'contact'},
    {title:"Trips History", pageName:"TripsPage",icon:'jet'},
    {title:"Induction", pageName:"InductionPage",icon:'leaf'}
  
     
    
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams,public cookieService:CookieService,public loginService:LoginService){
  }

 openPage(page:PageInterface){
  
 this.nav.setRoot(page.pageName);
 

 }

 logout(){
  this.cookieService.setItem('lmeout', null, null, null, null, null);
  this.nav.setRoot('LoginPage');
  
 }
 
 isActive(page:PageInterface){

  let childNav=this.nav.getActiveChildNav();
  if(childNav){
    if(childNav.getSelected() && childNav.getSelected().root===page.tabComponent){
      return 'primary';
    }
    return;
  }

  if(this.nav.getActive() && this.nav.getActive().name===page.pageName)
  {
    return 'primary';
  }

 }

}
