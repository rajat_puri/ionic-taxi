import { window } from 'rxjs/operator/window';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
 
 
import { environment } from '../../app/environments/environment';
  

import { CookieService } from '../../app/cookie.service';
@Injectable()
export class LoginService {

  constructor(private http: Http, private cookieService: CookieService) { }
  roles(userType) {
    return this.http.get(environment.baseURL + 'privilege/insert/' + userType)
      .map(res => res.json());
  }
  clear(){
   
    this.cookieService.removeItem("ID",null,null);
    this.cookieService.removeItem("token",null,null);
    this.cookieService.removeItem("userId",null,null);
 
  }
 
 
 
 
  authenticate(lDetails) {

    lDetails.email = lDetails.email.toLowerCase(); // bcz email and supposed to be in lowecare
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(environment.baseURL + 'authenticate', lDetails, { headers: headers })
      .map(res => res.json())
      
  }
  forgotPassword(email) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(environment.baseURL + '/forgotPassword', email, { headers: headers })
      .map(res => res.json());
  }
  verifyEmail(token) {
   
    return this.http.get(environment.baseURL + 'verifyEmail/SetEmailStatus/' + token)
      .map(res => res.json());
  }
 
 
   
 
  
 
   

 


}
